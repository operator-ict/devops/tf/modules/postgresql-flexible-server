variable "resource_group_name" {
  type    = string
  default = null
}

variable "name" {
  type = string
}

variable "tags" {
  type    = map(string)
  default = {}
}

variable "vnet_id" {
  type = string
}

variable "subnet_id" {
  type = string
}

variable "hub_vnet_id" {
  type = string
}

variable "location" {
  type = string
}

variable "availability_zone" {
  type = number
  validation {
    condition     = 1 <= var.availability_zone && var.availability_zone <= 3
    error_message = "The availability_zone value must be between 1 and 3"
  }
}

variable "sku" {
  type = string
}

variable "psql_version" {
  type = number
}

variable "storage_mb" {
  type = number
  validation {
    condition = contains(
      [32768, 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304, 8388608, 16777216, 33554432],
      var.storage_mb
    )
    error_message = "The storage_mb value must one of defined values"
  }
}

variable "backup_retention_days" {
  type    = number
  default = 7
}

variable "create_mode" {
  type    = string
  default = null
}

variable "admin_username" {
  type = string
}

variable "database_names" {
  type    = list(string)
  default = []
}

variable "configurations" {
  type    = map(string)
  default = {}
}

variable "private_dns_zone_name_prefix" {
  type    = string
  default = null
}

variable "public_network_access" {
  type = string
  default = false
}

variable "admin_ids" {
  type = list(string)
  default = []
}
