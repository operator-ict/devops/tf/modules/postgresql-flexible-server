resource "azurerm_resource_group" "rg" {
  name     = var.resource_group_name != null ? var.resource_group_name : "rg-psql-${var.name}"
  location = var.location
  tags     = var.tags
}

resource "azurerm_private_dns_zone" "pdz" {
  count               = var.public_network_access ? 0 : 1
  name                = var.private_dns_zone_name_prefix != null ? "${var.private_dns_zone_name_prefix}.postgres.database.azure.com" : "${var.name}.postgres.database.azure.com"
  resource_group_name = azurerm_resource_group.rg.name
  tags                = var.tags
}

resource "azurerm_private_dns_zone_virtual_network_link" "default" {
  count                 = var.public_network_access ? 0 : 1
  name                  = "dns-psql-${var.name}"
  private_dns_zone_name = azurerm_private_dns_zone.pdz[0].name
  virtual_network_id    = var.vnet_id
  resource_group_name   = azurerm_resource_group.rg.name
  tags                  = var.tags

  depends_on = [azurerm_private_dns_zone.pdz]
}

resource "azurerm_private_dns_zone_virtual_network_link" "hub" {
  count                 = var.public_network_access ? 0 : 1
  name                  = "dns-psql-${var.name}-hub"
  private_dns_zone_name = azurerm_private_dns_zone.pdz[0].name
  virtual_network_id    = var.hub_vnet_id
  resource_group_name   = azurerm_resource_group.rg.name
  tags                  = var.tags

  depends_on = [azurerm_private_dns_zone.pdz]
}

locals {
  kv-admin-pass-key = "psql-admin-password"

  psql_roles = [for admin_id in var.admin_ids : {
    principal_id   = admin_id
    role = "Contributor"
  }]

  kv_roles = [for admin_id in var.admin_ids : {
    principal_id   = admin_id
    role = "Key Vault Secrets User"
  }]
}

module "key_vault" {
  source                = "gitlab.com/operator-ict/key-vault/azurerm"
  version               = "0.3.1"
  name                  = "psql-${var.name}"
  resource_group_name   = azurerm_resource_group.rg.name
  location              = var.location
  expected_kv_passwords = [local.kv-admin-pass-key]
  depends_on            = [azurerm_private_dns_zone.pdz]
  roles                 = local.kv_roles
  tags                  = var.tags
}

resource "azurerm_postgresql_flexible_server" "psql" {
  name                          = "psql-${var.name}"
  resource_group_name           = azurerm_resource_group.rg.name
  location                      = var.location
  version                       = var.psql_version
  delegated_subnet_id           = var.public_network_access ? null : var.subnet_id
  private_dns_zone_id           = var.public_network_access ? null : azurerm_private_dns_zone.pdz[0].id
  administrator_login           = var.admin_username
  administrator_password        = module.key_vault.expected_secrets[local.kv-admin-pass-key].value
  public_network_access_enabled = var.public_network_access
  zone                          = var.availability_zone
  storage_mb                    = var.storage_mb
  sku_name                      = var.sku
  backup_retention_days         = var.backup_retention_days
  create_mode                   = var.create_mode
  tags                          = var.tags

  depends_on = [azurerm_private_dns_zone_virtual_network_link.default, module.key_vault]

  maintenance_window {
    day_of_week  = 1
    start_hour   = 2
    start_minute = 0
  }
}

resource "azurerm_postgresql_flexible_server_database" "psql_db" {
  for_each = toset(var.database_names)

  name      = each.value
  server_id = azurerm_postgresql_flexible_server.psql.id
  collation = "cs_CZ.utf8"
  charset   = "utf8"

  depends_on = [azurerm_postgresql_flexible_server.psql]
}

resource "azurerm_postgresql_flexible_server_configuration" "resource" {
  for_each   = var.configurations
  name       = each.key
  server_id  = azurerm_postgresql_flexible_server.psql.id
  value      = each.value
  depends_on = [azurerm_postgresql_flexible_server.psql]
}

resource "azurerm_role_assignment" "roles" {
  for_each = {
    for val in coalesce(local.psql_roles, []) : "${val.role}>${val.principal_id}" => val
  }

  scope                = azurerm_postgresql_flexible_server.psql.id
  role_definition_name = "Contributor"
  principal_id         = each.value.principal_id
}
