# Unreleased
## Added
## Changed
## Fixed

# [0.3.0] - 2025-01-08
## Changed
- Replace roles by admin_ids which sets also roles for key vault

# [0.2.6] - 2025-01-03
## Added
- Add roles and priviledges

# [0.2.5] - 2024-09-18
## Fixed
- For enabled public_network_access disable private DNS and disable subnet_id

# [0.2.4] - 2024-05-31
## Addded
- Property public_network_access_enabled added

# [0.2.3] - 2023-11-12
## Addded
- Property private_dns_zone_name_prefix - compatibility for restored servers
- Proporty resource_group_name - compatibility for restored servers

## Fixed
- Default create_mode to null

# [0.2.2] - 2023-11-08
## Changed
- Disable limitation to PSQL 14

## Added
- Add create_mode property

# [0.2.1] - 2023-01-02
## Added
- Add tags
